import time

from selenium.webdriver import ActionChains
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.common.keys import Keys

from Locators.LocatorsOfWebsite import locators


class PositionsPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.PositionsButton = locators.POSITIONS_BUTTON_BY_XPATH
        self.FirstPosition = locators.FIRST_POSITION_BY_XPATH
        self.StopLossButton = locators.STOP_LOSS_BUTTON_BY_XPATH
        self.EnableStopLossCheckbox = locators.ENABLE_STOP_LOSS_BY_XPATH
        self.StopLossPercentageField = locators.STOP_LOSS_PERCENTAGE_FIELD_BY_XPATH
        self.FollowingStopLoss = locators.FOLLOWING_STOP_LOSS_CHECKBOX_BY_XPATH
        self.SaveButton = locators.SAVE_LOSS_PROFIT_BUTTON_BY_XPATH

    def clickPositionsButton(self):
        self.driver.find_element_by_xpath(self.PositionsButton).click()

    def clickFirstPosition(self):
        self.driver.find_element_by_xpath(self.FirstPosition).click()

    def clickStopLossButton(self):
        self.driver.find_element_by_xpath(self.StopLossButton).click()

    def clickEnableStopLossButton(self):
        element = self.driver.find_element_by_xpath(self.EnableStopLossCheckbox)
        actions = ActionChains(self.driver)
        actions.move_to_element(element).click(element).perform()

    def clearStopLossField(self):
        self.driver.find_element_by_xpath(self.StopLossPercentageField).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath(self.StopLossPercentageField).send_keys(Keys.DELETE)

    def enterDataInStopLossField(self, StopLossPercentage):
        self.driver.find_element_by_xpath(self.StopLossPercentageField).send_keys(StopLossPercentage)

    def clickFollowingStopLossCheckBox(self):
        self.driver.find_element_by_xpath(self.FollowingStopLoss).click()

    def clickSaveButton(self):
        self.driver.find_element_by_xpath(self.SaveButton).click()
