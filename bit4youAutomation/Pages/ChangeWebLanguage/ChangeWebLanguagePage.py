from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators



class ChangeWebLanguage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ChangeLanguageButton = locators.CHANGE_LANGUAGE_BUTTON_By_Xpath
        self.FranciasButton = locators.FRANCAIS_LANGUAGE_BUTTON_By_Xpath
        self.NederlandsButton = locators.NEDERLANDS_LANGUAGE_BUTTON_By_Xpath
        self.EnglishButton = locators.ENGLISH_LANGUAGE_BUTTON_By_Xpath
        self.FranciasText = locators.FRANCIAS_LANGUAGE_TEXT_By_Xpath
        self.NederlandText = locators.NEDERLANDS_LANGUAGE_TEXT_By_Xpath
        self.EnglishText = locators.ENGLISH_LANGUAGE_TEXT_By_Xpath
        self.FranciasTextMainButton = locators.FRANCIAS_LANGUAGE__MAIN_BUTTON_By_Xpath

    def clickChangeLanguage(self):
        self.driver.find_element_by_xpath(self.ChangeLanguageButton).click()

    def clickFranciasChangeLanguage(self):
        self.driver.find_element_by_xpath(self.FranciasTextMainButton).click()

    def selectFranciasLanguage(self):
        self.driver.find_element_by_xpath(self.FranciasButton).click()

    def getFranciasText(self):
        FranciasHeadingText = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.FranciasText)))
        return FranciasHeadingText.text

    def selectNederlandLanguage(self):
        self.driver.find_element_by_xpath(self.NederlandsButton).click()

    def getNederlandText(self):
        NederlandHeadingText = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.NederlandText)))
        return NederlandHeadingText.text

    def selectEnglishLanguage(self):
        self.driver.find_element_by_xpath(self.EnglishButton).click()

    def getEnglishBasedWebsiteURL(self):
        EnglishTranslatedURL = self.driver.current_url
        return EnglishTranslatedURL
