from Locators.LocatorsOfWebsite import locators


class BuyAndSellCryptoAssetsVerifyYourAccount(locators):

    def __init__(self, driver):
        self.driver = driver

        self.VerifyYourAccount = locators.VERIFY_YOUR_ACCOUNT_BUTTON_By_Xpath

    def clickVerifyYourAccountButton(self):
        self.driver.find_element_by_xpath(self.VerifyYourAccount).click()
