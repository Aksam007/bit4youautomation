from Locators.LocatorsOfWebsite import locators


class BuyAndSellCryptoAssets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyAndSellCryptoAssets = locators.BUY_AND_SELL_CRYPTO_ASSETS_BUTTON_By_Xpath

    def clickBuyAndSellCryptoAssetsButton(self):
        self.driver.find_element_by_xpath(self.BuyAndSellCryptoAssets).click()
