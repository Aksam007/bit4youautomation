from Locators.LocatorsOfWebsite import locators


class BuyAndSellCryptoPageCreateAFreeAccount(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyAndSellCryptoCreateAFreeAccount = locators.CREATE_A_FREE_ACCOUNT_BUTTON_By_Xpath

    def clickBuyAndSellCryptoCreateAFreeAccountButton(self):
        self.driver.find_element_by_xpath(self.BuyAndSellCryptoCreateAFreeAccount).click()
