from Locators.LocatorsOfWebsite import locators


class BuyAndSellCryptoPageCreateAnAccount(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyAndSellCryptoCreateAnAccount = locators.CREATE_AN_ACCOUNT_BUTTON_By_Xpath

    def clickBuyAndSellCryptoCreateAnAccountButton(self):
        self.driver.find_element_by_xpath(self.BuyAndSellCryptoCreateAnAccount).click()
