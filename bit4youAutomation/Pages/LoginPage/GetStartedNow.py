from Locators.LocatorsOfWebsite import locators


class GetStartedNow(locators):

    def __init__(self, driver):
        self.driver = driver

        self.GetStartedNowButton = locators.GET_STARTED_NOW_BUTTON_By_Xpath

    def clickGetStartedNowButton(self):
        self.driver.find_element_by_xpath(self.GetStartedNowButton).click()
