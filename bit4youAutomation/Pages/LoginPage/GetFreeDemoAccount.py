from Locators.LocatorsOfWebsite import locators


class GetFreeDemoAccount(locators):

    def __init__(self, driver):
        self.driver = driver

        self.GetFreeDemoAccountButton = locators.OPEN_AN_ACCOUNT_BUTTON_By_Xpath

    def clickGetFreeDemoAccountButton(self):
        self.driver.find_element_by_xpath(self.GetFreeDemoAccountButton).click()
