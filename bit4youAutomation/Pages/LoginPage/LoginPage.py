from Locators.LocatorsOfWebsite import locators
from twilio.rest import Client


class Login(locators):

    def __init__(self, driver):
        self.driver = driver

        self.LoginButtonOnHomePage = locators.LOGIN_BUTTON_ON_HOME_PAGE_By_Xpath
        self.LoginEmailOrPhone = locators.LOGIN_EMAIL_OR_PHONE_By_Xpath
        self.LoginPassword = locators.LOGIN_PASSWORD_By_xpath
        self.LoginButtonOnLoginPage = locators.LOGIN_BUTTON_By_Xpath
        self.LoginPageOTP = locators.OTP_INPUT_BY_XPATH
        self.ContinueButtonOnLoginPageOTP = locators.CONTINUE_BUTTON_BY_XPATH

    def clickLoginButtonOnHomepage(self):
        self.driver.find_element_by_xpath(self.LoginButtonOnHomePage).click()

    def loginUserEmailOrPhone(self, userEmailOrPhone):
        self.driver.find_element_by_xpath(self.LoginEmailOrPhone).send_keys(userEmailOrPhone)

    def loginPassword(self, pin):
        self.driver.find_element_by_xpath(self.LoginPassword).send_keys(pin)

    def clickLoginButtonOnLoginPage(self):
        self.driver.find_element_by_xpath(self.LoginButtonOnLoginPage).click()

    def getOtpMessage(self):
        OTP = ""
        account_sid = "ACcfea74db725f357196f1af44e9745e50"
        auth_token = "6e83080f7bcc4f0096ac6d8b9d8db975"
        client = Client(account_sid, auth_token)
        messages = client.messages.list(to='+12083016348', limit=1)
        for sms in messages:
            OTPOne = sms.body.split(" ")[0]
            OTP = OTPOne
        self.driver.find_element_by_xpath(self.LoginPageOTP).send_keys(OTP)

    def clickContinueButton(self):
        self.driver.find_element_by_xpath(self.ContinueButtonOnLoginPageOTP).click()

