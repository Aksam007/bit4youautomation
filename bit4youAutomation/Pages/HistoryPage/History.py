from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class HistoryPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.HistoryButton = locators.HISTORY_BUTTON_BY_XPATH
        self.PositionsHistory = locators.POSITIONS_HISTORY_BY_XPATH
        self.OrdersHistory = locators.ORDERS_HISTORY_BY_XPATH
        self.FundsHistory = locators.FUNDS_HISTORY_BY_XPATH
        self.BlockchainHistory = locators.BLOCK_CHAIN_HISTORY_BY_XPATH
        self.ExportHistory = locators.EXPORT_HISTORY_BY_XPATH

    def hoverOnHistoryButton(self):
        actions = ActionChains(self.driver)
        ImageLink = self.driver.find_element_by_xpath(self.HistoryButton)
        actions.move_to_element(ImageLink)
        actions.perform()

    def clickOnPositionsHistory(self):
        self.driver.find_element_by_xpath(self.PositionsHistory).click()

    def clickOnOrdersHistory(self):
        self.driver.find_element_by_xpath(self.OrdersHistory).click()

    def clickOnFundsHistory(self):
        self.driver.find_element_by_xpath(self.FundsHistory).click()

    def clickOnBlockchainHistory(self):
        self.driver.find_element_by_xpath(self.BlockchainHistory).click()

    def clickOnExportHistory(self):
        self.driver.find_element_by_xpath(self.ExportHistory).click()


