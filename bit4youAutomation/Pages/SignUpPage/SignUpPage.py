from Locators.LocatorsOfWebsite import locators


class SignUp(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SignUpButtonOnHomePage = locators.SIGN_UP_BUTTON_ON_HOME_PAGE_By_Xpath
        self.SignUpFirstName = locators.SIGN_UP_FIRST_NAME_By_Xpath
        self.SignUpPhoneNumber = locators.SIGN_UP_PHONE_NUMBER_By_Xpath
        self.SignUpPassword = locators.SIGN_UP_PASSWORD_By_Xpath
        self.SignUpConfirmPassword = locators.SIGN_UP_CONFIRM_PASSWORD_By_Xpath
        self.SignTermsOfServiceButton = locators.TERMS_OF_SERVICES_BUTTON_By_Xpath
        self.PrivacyAndCookieButton = locators.PRIVACY_AND_COOKIE_BUTTON_By_Xpath
        self.RegisterButton = locators.REGISTER_BUTTON_By_xpath

    def clickSignUpButtonOnHomepage(self):
        self.driver.find_element_by_xpath(self.SignUpButtonOnHomePage).click()

    def SignUpFirstName(self, firstname):
        self.driver.find_element_by_xpath(self.SignUpFirstName).send_keys(firstname)

    def SignUpUserPhone(self, userPhone):
        self.driver.find_element_by_xpath(self.SignUpPhoneNumber).send_keys(userPhone)

    def SignUpPassword(self, password):
        self.driver.find_element_by_xpath(self.SignUpPassword).send_keys(password)

    def SignUpConfirmPassword(self, confirmPassword):
        self.driver.find_element_by_xpath(self.SignUpConfirmPassword).send_keys(confirmPassword)

    def clickTermsOfServiceCheckBox(self):
        self.driver.find_element_by_xpath(self.SignTermsOfServiceButton).click()

    def clickPrivacyAndCookieCheckBox(self):
        self.driver.find_element_by_xpath(self.PrivacyAndCookieButton).click()

    def clickRegisterButton(self):
        self.driver.find_element_by_xpath(self.RegisterButton).click()
