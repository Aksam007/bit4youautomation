import time

from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class OurAssetSectionAllButtons(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OurAssetsButton = locators.OUR_ASSETS_BUTTONS_By_Xpath

    def clickAllOurAssetsButton(self):
        number_of_button = len(WebDriverWait(self.driver, 10).until(
            EC.visibility_of_all_elements_located((By.XPATH, self.OurAssetsButton))))
        print(number_of_button)
        for x in range(number_of_button):
            button = self.driver.find_element_by_xpath("/descendant::div[@class='flex currency-logo xs6 justify-center align-center'][" + str(x+1) + "]")
            button.click()
            time.sleep(5)
            self.driver.execute_script("window.history.go(-1)")
            time.sleep(5)
