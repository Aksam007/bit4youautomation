from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BlockChainRevolution(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BitCoinButtonOnOurAssets = locators.OUR_ASSETS_BUTTONS_By_Xpath
        self.BlockChainRevolutionButton = locators.BLOCK_CHAIN_REVOLUTION_By_Xpath
        self.BlockChainRevolutionMainHeading = locators.BLOCK_CHAIN_MAIN_TITLE_By_Xpath

    def clickBitcoinButton(self):
        BitcoinButton = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((By.XPATH, self.BitCoinButtonOnOurAssets)))
        BitcoinButton.click()

    def clickBlockChainRevolutionButton(self):
        BlockChainRevolutionBtn = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((By.XPATH, self.BlockChainRevolutionButton)))
        BlockChainRevolutionBtn.click()

    def getBlockChainRevolutionMainHeading(self):
        BlockChainRevolutionHeading = self.driver.find_element_by_xpath(self.BlockChainRevolutionMainHeading)
        # WebDriverWait(self.driver, 2).until(
        # EC.presence_of_element_located((By.XPATH, self.BlockChainRevolutionMainHeading)))
        return BlockChainRevolutionHeading.text
