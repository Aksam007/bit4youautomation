
from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class LiteCoinButtonOnOurAsset(locators):

    def __init__(self, driver):
        self.driver = driver

        self.LiteCoinButtonOnOurAssets = locators.LITECOIN_BUTTON_ON_ASSETS_By_Xpath
        self.BuyLiteCoinNowButton = locators.LITECOIN_BITCOIN_NOW_BUTTON_By_Xpath

    def clickLiteCoinButton(self):
        LiteCoinButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.LiteCoinButtonOnOurAssets)))
        LiteCoinButton.click()

    def clickBuyLiteCoinNowButton(self):
        BuyLiteCoinNowButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.BuyLiteCoinNowButton)))
        BuyLiteCoinNowButton.click()