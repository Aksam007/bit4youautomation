
from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class EthereumButtonOnOurAsset(locators):

    def __init__(self, driver):
        self.driver = driver

        self.EthereumButtonOnOurAssets = locators.ETHEREUM_BUTTON_ON_ASSETS_By_Xpath
        self.BuyEthereumNowButton = locators.ETHEREUM_BITCOIN_NOW_BUTTON_By_Xpath

    def clickEthereumButton(self):
        EthereumButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.EthereumButtonOnOurAssets)))
        EthereumButton.click()

    def clickBuyEthereumNowButton(self):
        BuyEthereumNowButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.BuyEthereumNowButton)))
        BuyEthereumNowButton.click()