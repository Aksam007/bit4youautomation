
from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class RippleButtonOnOurAsset(locators):

    def __init__(self, driver):
        self.driver = driver

        self.RippleButtonOnOurAssets = locators.RIPPLE_BUTTON_ON_ASSETS_By_Xpath
        self.RippleCoinNowButton = locators.RIPPLE_BITCOIN_NOW_BUTTON_By_Xpath

    def clickRippleButton(self):
        RippleButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.RippleButtonOnOurAssets)))
        RippleButton.click()

    def clickBuyRippleCoinNowButton(self):
        BuyRippleNowButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.RippleCoinNowButton)))
        BuyRippleNowButton.click()