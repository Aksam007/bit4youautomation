import time

from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BitcoinButtonOnOurAsset(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BitCoinButtonOnOurAssets = locators.OUR_ASSETS_BUTTONS_By_Xpath
        self.BuyBitcoinNowButton = locators.BUY_BITCOIN_NOW_BUTTON_By_Xpath

    def clickBitcoinButton(self):
        BitcoinButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.BitCoinButtonOnOurAssets)))
        BitcoinButton.click()

    def clickBuyBitcoinNowButton(self):
        BuyBitcoinNowButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.BuyBitcoinNowButton)))
        BuyBitcoinNowButton.click()