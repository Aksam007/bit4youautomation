from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class News(locators):

    def __init__(self, driver):
        self.driver = driver

        self.NewsButton = locators.NEWS_BUTTON_ON_HOME_PAGE_By_Xpath
        self.NewsPageHeading = locators.NEWS_MAIN_HEADING_By_Xpath
        self.ReadMoreButtonOnArticle = locators.CRYPTO_NEWS_READ_MORE_BUTTON_By_Xpath

    def clickNewsButton(self):
        self.driver.find_element_by_xpath(self.NewsButton).click()

    def getNewsPageHeading(self):
        NewsHeading = WebDriverWait(self.driver, 15).until(EC.presence_of_element_located((By.XPATH, self.NewsPageHeading)))
        return NewsHeading.text

    def clickReadMoreButton(self):
        ReadMoreButton = WebDriverWait(self.driver, 15).until(EC.presence_of_element_located((By.XPATH, self.ReadMoreButtonOnArticle)))
        ReadMoreButton.click()