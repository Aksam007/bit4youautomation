import time

from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class NewsPageAllCategories(locators):

    def __init__(self, driver):
        self.driver = driver

        self.NewsButton = locators.NEWS_BUTTON_ON_HOME_PAGE_By_Xpath
        self.AllCategoriesButton = locators.ALL_CATEGORIES_BUTTON_By_Xpath

    def clickNewsButton(self):
        self.driver.find_element_by_xpath(self.NewsButton).click()

    def clickAllCategoriesButton(self):
        number_of_buttons = len(WebDriverWait(self.driver, 10).until(
            EC.visibility_of_all_elements_located((By.XPATH, self.AllCategoriesButton))))
        for x in range(number_of_buttons):
            button = self.driver.find_element_by_xpath("/descendant::div[@class='my-2 v-list-item v-list-item--link theme--light'][" + str(x+1) + "]")
            button.click()
            time.sleep(5)
