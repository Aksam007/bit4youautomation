from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators


class NewBeeProBank(locators):

    def __init__(self, driver):
        self.driver = driver

        self.NewBeeStartDemoButton = locators.NEWBEE_DEMO_BUTTON_By_Xpath
        self.ProGetStartedButton = locators.PRO_GET_STARTED_NOW_BUTTON_By_Xpath
        self.BankContactUsButton = locators.BANK_CONTACT_US_BUTTON_By_Xpath

    def clickNewBeeStartDemoButton(self):
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.NewBeeStartDemoButton))).click()

    def clickProGetStartedButton(self):
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.ProGetStartedButton))).click()

    def clickBankContactUsButton(self):
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.BankContactUsButton))).click()