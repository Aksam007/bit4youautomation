from Locators.LocatorsOfWebsite import locators


class Logout(locators):

    def __init__(self, driver):
        self.driver = driver

        self.LogoutButtonOnDashboard = locators.LOGOUT_BUTTON_BY_XPATH

    def clickLogoutButton(self):
        self.driver.find_element_by_xpath(self.LogoutButtonOnDashboard).click()