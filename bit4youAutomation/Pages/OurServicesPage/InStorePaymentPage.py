from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class OurServicesInStorePayment(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ViewMerchantToolsButton = locators.VIEW_MERCHANT_TOOLS_BUTTON_By_Xpath
        self.MerchantToolHeading = locators.MERCHANT_TOOLS_MAIN_HEADING_By_Xpath
        self.GetInTouchButton = locators.GET_IN_TOUCH_BUTTON_By_Xpath
        self.ContactUsForm = locators.CONTACT_US_FORM_By_Xpath

    def clickViewMerchantToolsButton(self):
        self.driver.find_element_by_xpath(self.ViewMerchantToolsButton).click()

    def getMerchantToolHeading(self):
        MerchantToolMainHeading = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.MerchantToolHeading)))
        return MerchantToolMainHeading.text

    def clickGetInTouchButton(self):
        GetInTouchButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.GetInTouchButton)))
        GetInTouchButton.click()

    def getContactUsFormHeading(self):
        ContactUsFormHeading = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.ContactUsForm)))
        return ContactUsFormHeading.text
