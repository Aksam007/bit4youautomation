from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators


class OurServicesFiatWithdrawal(locators):

    def __init__(self, driver):
        self.driver = driver

        self.FiatWithdrawalButton = locators.FIAT_WITHDRAWAL_WHICH_FEES_By_Xpath
        self.DepositFee = locators.DEPOSIT_FEES_By_Xpath
        self.WithdrawalFee = locators.WITHDRAWAL_FEES_By_Xpath
        self.CryptoToCrptyoExchange = locators.CRYPTO_TO_CRYPTO_FEES_By_Xpath

    def clickFiatWithdrawalButton(self):
        self.driver.find_element_by_xpath(self.FiatWithdrawalButton).click()

    def getDepositFee(self):
        DepositFee = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.DepositFee)))
        return DepositFee.text

    def getWithdrawalFee(self):
        WithdrawalFee = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.WithdrawalFee)))
        return WithdrawalFee.text

    def getCrypto_to_CryptoExchangeFee(self):
        CryptoToCryptoExchange = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.CryptoToCrptyoExchange)))
        return CryptoToCryptoExchange.text
