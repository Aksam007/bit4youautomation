from Locators.LocatorsOfWebsite import locators


class OurServicesDemo(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OurServicesDemoButton = locators.OUR_SERVICES_DEMO_BUTTON_By_Xpath

    def clickCreateAnAccountButton(self):
        self.driver.find_element_by_xpath(self.OurServicesDemoButton).click()
