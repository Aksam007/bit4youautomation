from Locators.LocatorsOfWebsite import locators


class OurServicesWallets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OurServicesWalletsAvailableCryptoButton = locators.WALLETS_AVAILABLE_CRYPTOS_BUTTON_By_Xpath

    def clickAvailableCryptoButton(self):
        self.driver.find_element_by_xpath(self.OurServicesWalletsAvailableCryptoButton).click()
