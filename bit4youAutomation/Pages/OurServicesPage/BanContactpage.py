from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators


class OurServicesBanContact(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OurServicesBanContactUSTDButton = locators.BANCONTACT_WHAT_IS_USDT_BUTTON_By_Xpath
        self.USTDLogo = locators.USTD_LOGO_By_Xpath
        self.MainHeading = locators.USTD_MAIN_HEADING_By_Xpath
        self.ReadTheWhitePaperButton = locators.READ_THE_WHITE_PAPER_BUTTON_By_Xpath

    def clickWhatIsUSTDButton(self):
        self.driver.find_element_by_xpath(self.OurServicesBanContactUSTDButton).click()

    def getUSTDMainHeading(self):
        USTDMainHeading = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.MainHeading)))
        return USTDMainHeading.text

    def clickReadWhitePaperButton(self):
        ReadWhitePaperButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.ReadTheWhitePaperButton)))
        ReadWhitePaperButton.click()
        self.driver.execute_script("window.history.go(-1)")