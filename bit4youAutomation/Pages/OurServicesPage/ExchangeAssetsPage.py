import time

from selenium.webdriver.common.keys import Keys

from Locators.LocatorsOfWebsite import locators


class OurServicesExchangeAssets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OurServicesExchangeAssetsButton = locators.EXCHANGE_ASSETS_BUTTON_By_Xpath
        self.ExchangeAssetsAmount = locators.EXCHANGE_ASSETS_AMOUNT_By_Xpath
        self.ExchangeAssetsChangeFromDropDown = locators.EXCHANGE_ASSETS_CHANGE_FROM_By_Xpath
        self.ExchangeAssetsFromValue = locators.EXCHANGE_ASSETS_FROM_VALUE_By_Xpath
        self.ExchangeAssetsChangeToDropDown = locators.EXCHANGE_ASSETS_CHANGE_TO_By_Xpath
        self.ExchangeAssetsToValue = locators.EXCHANGE_ASSETS_TO_VALUE_By_Xpath

    def clickExchangeAssetsButton(self):
        self.driver.find_element_by_xpath(self.OurServicesExchangeAssetsButton).click()

    def EnterAmount(self, amount):
        AmountField = self.driver.find_element_by_xpath(self.ExchangeAssetsAmount)
        AmountField.click()
        AmountField.send_keys(Keys.CONTROL + "a")
        AmountField.send_keys(Keys.DELETE)
        AmountField.send_keys(amount)

    def SelectValueFrom_FromDropDown(self):
        self.driver.find_element_by_xpath(self.ExchangeAssetsChangeFromDropDown).click()
        time.sleep(5)
        self.driver.find_element_by_xpath(self.ExchangeAssetsFromValue).click()

    def SelectValueFrom_ToDropDown(self):
        self.driver.find_element_by_xpath(self.ExchangeAssetsChangeToDropDown).click()
        time.sleep(5)
        self.driver.find_element_by_xpath(self.ExchangeAssetsToValue).click()