from selenium.common.exceptions import NoSuchElementException

from Locators.LocatorsOfWebsite import locators


class OpenOrderPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.OpenOrdersButton = locators.OPEN_ORDERS_BUTTON_BY_XPATH
        self.FirstOpenOrder = locators.FIRST_OPEN_ORDER_BY_XPATH
        self.CancelOrderButton = locators.CANCEL_ORDER_BUTTON_BY_XPATH

    def clickOpenOrdersButton(self):
        self.driver.find_element_by_xpath(self.OpenOrdersButton).click()

    def cancelFirstOrder(self):
        try:
            element = self.driver.find_element_by_xpath(self.FirstOpenOrder)
            if element.is_displayed():
                self.driver.find_element_by_xpath(self.FirstOpenOrder).click()
                self.driver.find_element_by_xpath(self.CancelOrderButton).click()
        except NoSuchElementException:
            print("No element found")



