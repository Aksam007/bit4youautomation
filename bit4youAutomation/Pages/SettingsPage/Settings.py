from selenium.webdriver.common.keys import Keys

from Locators.LocatorsOfWebsite import locators


class SettingsPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SettingsButton = locators.SETTINGS_BUTTON_BY_XPATH
        self.CurrencyDropdown = locators.CURRENCY_DROPDOWN_BY_XPATH
        self.EuroCurrency = locators.CHANGE_CURRENCY_TO_EURO_BY_XPATH
        self.CurrencyOnDashboard = locators.CURRENCY_ON_DASHBOARD_BY_XPATH
        self.LanguageDropdown = locators.LANGUAGE_DROPDOWN_BY_XPATH
        self.NederlandsLanguage = locators.CHANGE_LANGUAGE_TO_NEDERLANDS_BY_XPATH
        self.EnglishLanguage = locators.CHANGE_LANGUAGE_TO_ENGLISH_BY_XPATH
        self.LanguageOnDashboard = locators.LANGUAGE_ON_DASHBOARD_BY_XPATH
        self.ReferralButton = locators.REFERRAL_BUTTON_BY_XPATH
        self.ReferralAmount = locators.REFERRAL_AMOUNT_FIELD_BY_XPATH
        self.SaveButton = locators.SAVE_REFERRAL_AMOUNT_BUTTON_BY_XPATH
        self.SignOutButton = locators.SIGN_OUT_BUTTON_BY_XPATH

    def clickSettingsButton(self):
        self.driver.find_element_by_xpath(self.SettingsButton).click()

    def clickCurrencyDropdown(self):
        self.driver.find_element_by_xpath(self.CurrencyDropdown).click()

    def selectEuro(self):
        self.driver.find_element_by_xpath(self.EuroCurrency).click()

    def getCurrencyOnDashboard(self):
        currencyOnDashboard = self.driver.find_element_by_xpath(self.CurrencyOnDashboard)
        return currencyOnDashboard.text

    def clickLanguageDropdown(self):
        self.driver.find_element_by_xpath(self.LanguageDropdown).click()

    def selectNederlands(self):
        self.driver.find_element_by_xpath(self.NederlandsLanguage).click()

    def selectEnglish(self):
        self.driver.find_element_by_xpath(self.EnglishLanguage).click()

    def getLanguageOnDashboard(self):
        LanguageOnDashboard = self.driver.find_element_by_xpath(self.LanguageOnDashboard)
        return LanguageOnDashboard.text

    def clickReferralsButton(self):
        self.driver.find_element_by_xpath(self.ReferralButton).click()

    def setReferralAmount(self, ReferralTotalAmount):
        self.driver.find_element_by_xpath(self.ReferralAmount).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath(self.ReferralAmount).send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath(self.ReferralAmount).send_keys(ReferralTotalAmount)

    def clickSaveButton(self):
        self.driver.find_element_by_xpath(self.SaveButton).click()

    def clickSignOutButton(self):
        self.driver.find_element_by_xpath(self.SignOutButton).click()
