from Locators.LocatorsOfWebsite import locators


class WithdrawFundsPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.WithdrawFundsButton = locators.WITHDRAW_FUNDS_BUTTON_BY_XPATH

    def clickWithdrawFundsButton(self):
        self.driver.find_element_by_xpath(self.WithdrawFundsButton).click()
