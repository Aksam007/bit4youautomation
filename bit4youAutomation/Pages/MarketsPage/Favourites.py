from Locators.LocatorsOfWebsite import locators


class FavouriteMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.FavouriteMarketsButton = locators.FAVOURITE_BUTTON_BY_XPATH
        self.AddMarketButton = locators.ADD_MARKETS_BUTTON_BY_XPATH
        self.AddFavouriteMarketDropDown = locators.ADD_FAVOURITE_MARKET_DROPDOWN_BUTTON_BY_XPATH
        self.SaveButton = locators.SAVE_BUTTON_BY_XPATH
        self.UnfavouriteButton = locators.UNFAVOURITE_BUTTON_BY_XPATH

    def clickFavourites(self):
        self.driver.find_element_by_xpath(self.FavouriteMarketsButton).click()

    def clickAddMarket(self):
        self.driver.find_element_by_xpath(self.AddMarketButton).click()

    def clickAddFavouriteMarketDropDown(self):
        self.driver.find_element_by_xpath(self.AddFavouriteMarketDropDown).click()
        self.driver.find_element_by_xpath("//div[contains(text(),'USDT-BTC')]").click()
        self.driver.find_element_by_xpath(self.SaveButton).click()

    def clickUnfavouriteButton(self):
        self.driver.find_element_by_xpath(self.UnfavouriteButton).click()

