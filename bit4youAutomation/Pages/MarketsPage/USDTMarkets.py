import time

from Locators.LocatorsOfWebsite import locators


class USTDMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.USDTMarketButton = locators.USDT_MARKET_BUTTON_BY_XPATH
        self.FavouriteStarButton = locators.FAVOURITE_STAR_BUTTON_BY_XPATH
        self.USDTAllMarketsButton = locators.USDT_ALL_MARKETS_BY_XPATH

    def clickUSDTMarketButton(self):
        self.driver.find_element_by_xpath(self.USDTMarketButton).click()

    def clickOnFavouriteStartButton(self):
        self.driver.find_element_by_xpath(self.FavouriteStarButton).click()

    def clickOnAllUSDTMarkets(self):
        NoOfButtons = len(self.driver.find_elements_by_xpath(self.USDTAllMarketsButton))
        for Num in range(NoOfButtons):
            button = self.driver.find_element_by_xpath(
                "/descendant::a[@class='marketsRow'][" + str(Num + 1) + "]")
            button.click()
            time.sleep(5)
            self.driver.execute_script("window.history.go(-1)")
            time.sleep(5)


