import time

from Locators.LocatorsOfWebsite import locators


class ETHMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ETHMarketButton = locators.ETH_MARKET_BUTTON_BY_XPATH
        self.ETHAllMarketsButton = locators.USDT_ALL_MARKETS_BY_XPATH

    def clickETHMarketButton(self):
        self.driver.find_element_by_xpath(self.ETHMarketButton).click()

    def clickOnAllETHMarkets(self):
        NoOfButtons = len(self.driver.find_elements_by_xpath(self.ETHAllMarketsButton))
        for Num in range(NoOfButtons):
            button = self.driver.find_element_by_xpath(
                "/descendant::a[@class='marketsRow'][" + str(Num + 1) + "]")
            button.click()
            time.sleep(5)
            self.driver.execute_script("window.history.go(-1)")
            time.sleep(5)
