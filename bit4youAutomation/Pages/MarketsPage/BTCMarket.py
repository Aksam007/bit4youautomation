import time

from Locators.LocatorsOfWebsite import locators


class BTCMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BTCMarketButton = locators.BTC_MARKET_BUTTON_BY_XPATH
        self.BTCAllMarketsButton = locators.USDT_ALL_MARKETS_BY_XPATH

    def clickBTCMarketButton(self):
        self.driver.find_element_by_xpath(self.BTCMarketButton).click()

    def clickOnAllBTCMarkets(self):
        NoOfButtons = len(self.driver.find_elements_by_xpath(self.BTCAllMarketsButton))
        for Num in range(NoOfButtons):
            button = self.driver.find_element_by_xpath(
                "/descendant::a[@class='marketsRow'][" + str(Num + 1) + "]")
            button.click()
            time.sleep(5)
            self.driver.execute_script("window.history.go(-1)")
            time.sleep(5)
