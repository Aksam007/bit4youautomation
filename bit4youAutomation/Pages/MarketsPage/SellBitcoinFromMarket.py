import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators


class SellBitcoinFromUSTDMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyBitcoinFromUSDTMarket = locators.USDT_MARKET_BITCOIN_BY_XPATH
        self.SellButton = locators.SELL_BUTTON_BY_XPATH
        self.QuantityToBeSold = locators.QUANTITY_TO_SELL_FIELD_BY_XPATH
        self.CreateSellOrder = locators.CREATE_SELL_ORDER_BY_XPATH
        self.confirmSellButton = locators.CONFIRM_SELL_BUTTON_BY_XPATH
        self.OkayButton = locators.OK_BUTTON_ON_SELL_BY_XPATH
        self.SecondOkayButton = locators.SECOND_OK_BUTTON_BY_XPATH
        self.CustomRate = locators.CUSTOM_RATE_FOR_SELL_AMOUNT_BY_XPATH

    def SellBitcoinInUSDTMarketOnMarketRate(self, amount):
        self.driver.find_element_by_xpath(self.BuyBitcoinFromUSDTMarket).click()
        self.driver.find_element_by_xpath(self.SellButton).click()
        self.driver.find_element_by_xpath(self.QuantityToBeSold).send_keys(amount)
        self.driver.find_element_by_xpath(self.CreateSellOrder).click()
        self.driver.find_element_by_xpath(self.confirmSellButton).click()
        oKayButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.OkayButton)))
        oKayButton.click()
        SecondOkayButton = self.driver.find_element_by_xpath(self.SecondOkayButton)
        SecondOkayButton.click()

    def SellBitcoinInUSDTMarketOnCustomRate(self, amount, customRate):
        self.driver.find_element_by_xpath(self.BuyBitcoinFromUSDTMarket).click()
        self.driver.find_element_by_xpath(self.SellButton).click()
        self.driver.find_element_by_xpath(self.QuantityToBeSold).send_keys(amount)
        time.sleep(5)
        OrderRate = self.driver.find_element_by_xpath(self.CustomRate)
        OrderRate.click()
        OrderRate.clear()
        OrderRate.send_keys(customRate)
        self.driver.find_element_by_xpath(self.CreateSellOrder).click()
        self.driver.find_element_by_xpath(self.confirmSellButton).click()
        oKayButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.OkayButton)))
        oKayButton.click()
        SecondOkayButton = self.driver.find_element_by_xpath(self.SecondOkayButton)
        SecondOkayButton.click()


