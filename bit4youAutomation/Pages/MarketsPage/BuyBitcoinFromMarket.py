import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Locators.LocatorsOfWebsite import locators


class BuyBitcoinFromUSTDMarkets(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyBitcoinFromUSDTMarket = locators.USDT_MARKET_BITCOIN_BY_XPATH
        self.Amount = locators.AMOUNT_FIELD_BY_XPATH
        self.CreateBuyOrder = locators.CREATE_BUY_ORDER_BUTTON_BY_XPATH
        self.ConfirmButton = locators.CONFIRM_ORDER_BUTTON_BY_XPATH
        self.OKButton = locators.OK_BUTTON_BY_XPATH
        self.CustomRate = locators.CUSTOM_RATE_AMOUNT_BY_XPATH

    def buyBitcoinOnUSDTMarketOnMarketRate(self, amount):
        self.driver.find_element_by_xpath(self.BuyBitcoinFromUSDTMarket).click()
        self.driver.find_element_by_xpath(self.Amount).send_keys(amount)
        self.driver.find_element_by_xpath(self.CreateBuyOrder).click()
        self.driver.find_element_by_xpath(self.ConfirmButton).click()
        oKButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.OKButton)))
        oKButton.click()

    def buyBitcoinOnUSDTMarketOnCustomRate(self, amount, customRate):
        self.driver.find_element_by_xpath(self.BuyBitcoinFromUSDTMarket).click()
        self.driver.find_element_by_xpath(self.Amount).send_keys(amount)
        time.sleep(5)
        OrderRate = self.driver.find_element_by_xpath(self.CustomRate)
        OrderRate.click()
        OrderRate.clear()
        OrderRate.send_keys(customRate)
        self.driver.find_element_by_xpath(self.CreateBuyOrder).click()
        self.driver.find_element_by_xpath(self.ConfirmButton).click()
        oKButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.OKButton)))
        oKButton.click()


