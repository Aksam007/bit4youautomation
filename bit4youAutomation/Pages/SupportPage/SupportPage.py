from Locators.LocatorsOfWebsite import locators


class Support(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SupportButton = locators.SUPPORT_BUTTON_By_Xpath
        self.SupportPageHeading = locators.SUPPORT_Heading_By_Xpath
        self.ContactUsFormName = locators.CONTACT_US_FORM_NAME_By_xpath
        self.ContactUsFormEmail = locators.CONTACT_US_FORM_EMAIL_By_xpath
        self.ContactUsFormSubject = locators.CONTACT_US_FORM_SUBJECT_By_xpath
        self.ContactUsFormMessage = locators.CONTACT_US_FORM_MESSAGE_By_xpath
        self.ContactUsFormSendButton = locators.SEND_MESSAGE_By_Xpath
        self.EmailSentSuccessMessage = locators.EMAIL_SENT_SUCCESS_MESSAGE_By_Xpath
        self.EmailRestrictionOfTenFailureMessage = locators.EMAIL_RESTRICTION_SUCCESS_MESSAGE_By_Xpath
        self.AskAQuestionTextBox = locators.Ask_A_Question_By_Xpath
        self.AskAQuestionFirstAppearedElement = locators.As_A_Question_First_Index_Element_By_Xpath
        self.SuggestedReadBox = locators.Suggested_Reads_By_xpath
        self.SuggestedReadCryptoAsset = locators.SUGGESTED_READ_CRYPTO_ASSETS_By_Xpath

    def clickSupportButton(self):
        self.driver.find_element_by_xpath(self.SupportButton).click()

    def getSupportPageHeading(self):
        SupportPageTitle = self.driver.find_element_by_xpath(self.SupportPageHeading)
        return SupportPageTitle.text

    def setContactFormData(self, name, email, subject, message):
        ContactFormName = self.driver.find_element_by_xpath(self.ContactUsFormName)
        ContactFormName.click()
        ContactFormName.send_keys(name)
        self.driver.find_element_by_xpath(self.ContactUsFormEmail).send_keys(email)
        self.driver.find_element_by_xpath(self.ContactUsFormSubject).send_keys(subject)
        self.driver.find_element_by_xpath(self.ContactUsFormMessage).send_keys(message)

    def clickSendMessage(self):
        self.driver.find_element_by_xpath(self.ContactUsFormSendButton).click()

    def validateEmailSuccessMessage(self):
        ContactFormSentSuccess = self.driver.find_element_by_xpath(self.EmailSentSuccessMessage)
        return ContactFormSentSuccess.text

    def validateEmailRestrictionAssertion(self):
        ContactFormRestrictionOfTenMessageAssertion = self.driver.find_element_by_xpath(
            self.EmailRestrictionOfTenFailureMessage)
        return ContactFormRestrictionOfTenMessageAssertion.is_displayed()

    def validateEmailRestrictionMessage(self):
        ContactFormRestrictionOfTenMessage = self.driver.find_element_by_xpath(self.EmailRestrictionOfTenFailureMessage)
        return ContactFormRestrictionOfTenMessage.text

    def AskAQuestion(self, questionText):
        AskAQuestionTextBoxOnSupportPage = self.driver.find_element_by_xpath(self.AskAQuestionTextBox)
        AskAQuestionTextBoxOnSupportPage.click()
        AskAQuestionTextBoxOnSupportPage.send_keys(questionText)

    def clickAppearedSuggestionInAskAQuestion(self):
        self.driver.find_element_by_xpath(self.AskAQuestionFirstAppearedElement).click()

    def clickCryptoAssetSuggestedRead(self):
        self.driver.find_element_by_xpath(self.SuggestedReadBox).click()

    def getCryptoAssetMainTitle(self):
        ParagraphMainTitle = self.driver.find_element_by_xpath(self.SuggestedReadCryptoAsset)
        return ParagraphMainTitle.text

