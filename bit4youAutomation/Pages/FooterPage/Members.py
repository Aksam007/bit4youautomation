from Locators.LocatorsOfWebsite import locators


class MembersInFooter(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SignUpButtonInFooter = locators.SIGN_UP_BUTTON_FOOTER_By_Xpath
        self.LoginButtonInFooter = locators.LOGIN_BUTTON_FOOTER_By_Xpath
        self.LostPasswordButtonInFooter = locators.LOST_PASSWORD_FOOTER_By_Xpath
        self.ServicesAndFeesButtonInFooter = locators.SERVICE_AND_FEE_FOOTER_By_Xpath

    def clickSignUpButtonInFooter(self):
        self.driver.find_element_by_xpath(self.SignUpButtonInFooter).click()

    def clickLoginButtonInFooter(self):
        self.driver.find_element_by_xpath(self.LoginButtonInFooter).click()

    def clickLostPasswordButtonInFooter(self):
        self.driver.find_element_by_xpath(self.LostPasswordButtonInFooter).click()

    def clickServicesAndFeeButtonInFooter(self):
        self.driver.find_element_by_xpath(self.ServicesAndFeesButtonInFooter).click()

