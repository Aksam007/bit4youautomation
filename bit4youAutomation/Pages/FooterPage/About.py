from Locators.LocatorsOfWebsite import locators


class AboutInFooter(locators):

    def __init__(self, driver):
        self.driver = driver

        self.AboutUsButtonInFooter = locators.ABOUT_US_BUTTON_FOOTER_By_Xpath
        self.TermsAndConditionButtonInFooter = locators.TERMS_AND_CONDITION_FOOTER_By_Xpath
        self.PrivacyPolicyButtonInFooter = locators.PRIVACY_POLICY_BUTTON_FOOTER_By_Xpath
        self.CookiePolicyButtonInFooter = locators.COOKIE_POLICY_BUTTON_FOOTER_By_Xpath
        self.ConsumerClaimsButtonInFooter = locators.CONSUMER_CLAIMS_BUTTON_By_Xpath
        self.SystemStatusButtonInFooter = locators.SYSTEM_STATUS_BUTTON_By_Xpath

    def clickAboutUsButtonInFooter(self):
        self.driver.find_element_by_xpath(self.AboutUsButtonInFooter).click()

    def clickTermsAndConditionButtonInFooter(self):
        self.driver.find_element_by_xpath(self.TermsAndConditionButtonInFooter).click()

    def clickPrivacyPolicyButtonInFooter(self):
        self.driver.find_element_by_xpath(self.PrivacyPolicyButtonInFooter).click()

    def clickCookiePolicyButtonInFooter(self):
        self.driver.find_element_by_xpath(self.CookiePolicyButtonInFooter).click()

    def clickConsumerClaimsButtonInFooter(self):
        self.driver.find_element_by_xpath(self.ConsumerClaimsButtonInFooter).click()

    def clickSystemStatusButtonInFooter(self):
        self.driver.find_element_by_xpath(self.SystemStatusButtonInFooter).click()

