from Locators.LocatorsOfWebsite import locators
from ConfigTestData.TestData import TestData


class SocialMediaButtonsInFooter(locators):

    def __init__(self, driver):
        self.driver = driver

        self.TwitterButtonInFooter = locators.TWITTER_BUTTON_BY_XPATH
        self.FacebookButtonInFooter = locators.FACEBOOK_BUTTON_BY_XPATH
        self.InstagramButtonInFooter = locators.INSTAGRAM_BUTTON_BY_XPATH
        self.GitlabButtonInFooter = locators.GITLAB_BUTTON_BY_XPATH

    def clickTwitterButtonInFooter(self):
        self.driver.find_element_by_xpath(self.TwitterButtonInFooter).click()
        TwitterPageLink = self.driver.current_url
        assert TwitterPageLink == TestData.Bit4youTwitterPageLink

    def clickFacebookButtonInFooter(self):
        self.driver.find_element_by_xpath(self.FacebookButtonInFooter).click()
        FacebookPageLink = self.driver.current_url
        assert FacebookPageLink == TestData.Bit4youFacebookPageLink

    def clickInstagramButtonInFooter(self):
        self.driver.find_element_by_xpath(self.InstagramButtonInFooter).click()
        InstagramPageLink = self.driver.current_url
        assert InstagramPageLink == TestData.Bit4youInstagramPageLink

    def clickGitlabButtonInFooter(self):
        self.driver.find_element_by_xpath(self.GitlabButtonInFooter).click()
        GitlabPageLink = self.driver.current_url
        assert GitlabPageLink == TestData.Bit4youGitlabPageLink
