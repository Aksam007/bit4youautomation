from Locators.LocatorsOfWebsite import locators


class ResourcesInFooter(locators):

    def __init__(self, driver):
        self.driver = driver

        self.HomeButtonInFooter = locators.HOME_BUTTON_FOOTER_By_Xpath
        self.NewsButtonInFooter = locators.NEWS_BUTTON_FOOTER_By_Xpath
        self.SupportButtonInFooter = locators.SUPPORT_BUTTON_FOOTER_By_Xpath
        self.APIDocsButtonInFooter = locators.API_DOCS_BUTTON_FOOTER_By_Xpath
        self.CurrencyConverterInFooter = locators.CURRENCY_CONVERTER_FOOTER_By_Xpath
        self.WhoIsIPInFooter = locators.WHOS_IP_FOOTER_By_Xpath
        self.OurMarketsButtonInFooter = locators.OUR_MARKETS_FOOTER_By_Xpath

    def clickHomeButtonInFooter(self):
        self.driver.find_element_by_xpath(self.HomeButtonInFooter).click()

    def clickNewsButtonInFooter(self):
        self.driver.find_element_by_xpath(self.NewsButtonInFooter).click()

    def clickSupportButtonInFooter(self):
        self.driver.find_element_by_xpath(self.SupportButtonInFooter).click()

    def clickAPIDocsButtonInFooter(self):
        self.driver.find_element_by_xpath(self.APIDocsButtonInFooter).click()

    def clickCurrencyConverterInFooter(self):
        self.driver.find_element_by_xpath(self.CurrencyConverterInFooter).click()

    def clickWhoisIpInFooter(self):
        self.driver.find_element_by_xpath(self.WhoIsIPInFooter).click()

    def clickOurMarketsButtonInFooter(self):
        self.driver.find_element_by_xpath(self.OurMarketsButtonInFooter).click()
