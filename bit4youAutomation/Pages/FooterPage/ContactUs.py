from Locators.LocatorsOfWebsite import locators


class ContactInFooter(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ContactButtonInFooter = locators.CONTACT_BUTTON_FOOTER_By_Xpath

    def clickContactButtonInFooter(self):
        self.driver.find_element_by_xpath(self.ContactButtonInFooter).click()