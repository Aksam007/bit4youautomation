from Locators.LocatorsOfWebsite import locators


class Video(locators):

    def __init__(self, driver):
        self.driver = driver

        self.VideoButton = locators.VIDEO_FRAME_By_Xpath

    def clickVideoFrame(self):
        self.driver.find_element_by_xpath(self.VideoButton).click()
