from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class WalletsPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.WalletsButton = locators.WALLETS_BUTTON_BY_XPATH
        self.BitcoinCashBox = locators.BITCOIN_CASH_IN_WALLET_BY_XPATH
        self.ReceiveAssetsButton = locators.RECEIVE_ASSETS_IN_WALLET_BY_XPATH
        self.SendAssetsButton = locators.SEND_ASSETS_FROM_WALLET_BY_XPATH
        self.BuyBitcoinCashButton = locators.BUY_BITCOIN_CASH_BY_XPATH
        self.SellBitcoinCashButton = locators.SELL_BITCOIN_CASH_BY_XPATH
        self.TransactionBox = locators.FIRST_TRANSACTION_BOX_BY_XPATH
        self.TransactionId = locators.TXID_OF_FIRST_TRANSACTION_BY_XPATH

    def clickWalletsButton(self):
        self.driver.find_element_by_xpath(self.WalletsButton).click()

    def clickBitCoinCashBox(self):
        self.driver.find_element_by_xpath(self.BitcoinCashBox).click()
        # BitcoinCash = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.BitcoinCashBox)))
        # BitcoinCash.click()

    def clickReceiveAssetsButton(self):
        self.driver.find_element_by_xpath(self.ReceiveAssetsButton).click()
        # ReceiveAssets = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.ReceiveAssetsButton)))
        # ReceiveAssets.click()

    def clickSendAssetsButton(self):
        self.driver.find_element_by_xpath(self.SendAssetsButton).click()
        # SendAssets = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.SendAssetsButton)))
        # SendAssets.click()

    def clickBuyBitcoinCashButton(self):
        self.driver.find_element_by_xpath(self.BuyBitcoinCashButton).click()
        # BuyBitcoinCash = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.BuyBitcoinCashButton)))
        # BuyBitcoinCash.click()

    def clickSellBitcoinCashButton(self):
        self.driver.find_element_by_xpath(self.SellBitcoinCashButton).click()
        # SellBitcoinCash = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.SellBitcoinCashButton)))
        # SellBitcoinCash.click()

    def clickTransactionBox(self):
        self.driver.find_element_by_xpath(self.TransactionBox).click()
        # Transaction = WebDriverWait(self.driver, 15).until(
        #     EC.element_to_be_clickable((By.ID, self.TransactionBox)))
        # Transaction.click()

    def getTransactionIdFromFirstTransaction(self):
        TXID = self.driver.find_element_by_xpath(self.TransactionId)
        return TXID.text
