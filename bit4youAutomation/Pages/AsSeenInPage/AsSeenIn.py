import time

from Locators.LocatorsOfWebsite import locators
from ConfigTestData.TestData import TestData


class AsSeenInArticles(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SevenSurSevenArticle = locators.SEVEN_SUR_SEVEN_ARTICLE_BY_XPATH
        self.TrendsTendanceArticle = locators.TRENDS_TENDANCE_ARTICLE_BY_XPATH
        self.CanalZArticle = locators.CANAL_Z_ARTICLE_BY_XPATH
        self.LeSoirArticle = locators.LE_SOIR_ARTICLE_BY_XPATH
        self.laLibreArticle = locators.LA_LIBRE_ARTICLE_BY_XPATH
        self.LN24Article = locators.LN24_ARTICLE_BY_XPATH

    def clickSevenSurButton(self):
        self.driver.find_element_by_xpath(self.SevenSurSevenArticle).click()

    def clickTrendsTendanceButton(self):
        self.driver.find_element_by_xpath(self.TrendsTendanceArticle).click()

    def clickCanalZButton(self):
        self.driver.find_element_by_xpath(self.CanalZArticle).click()

    def clickLeSoirButton(self):
        self.driver.find_element_by_xpath(self.LeSoirArticle).click()

    def clicklaLibreButton(self):
        self.driver.find_element_by_xpath(self.laLibreArticle).click()

    def clickLN24Button(self):
        self.driver.find_element_by_xpath(self.LN24Article).click()

