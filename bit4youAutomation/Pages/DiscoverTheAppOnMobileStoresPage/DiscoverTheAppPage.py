from Locators.LocatorsOfWebsite import locators


class DiscoverTheAppOnStores(locators):

    def __init__(self, driver):
        self.driver = driver

        self.AppleStoreButton = locators.APPLE_STORE_LINK_BUTTON_By_Xpath
        self.GoogleStoreButton = locators.GOOGLE_STORE_LINK_BUTTON_By_Xpath

    def clickAppleStoreButton(self):
        self.driver.find_element_by_xpath(self.AppleStoreButton).click()
        AppleStoreUrl = self.driver.current_url
        return AppleStoreUrl

    def clickGoogleStoreButton(self):
        self.driver.find_element_by_xpath(self.GoogleStoreButton).click()
        GoogleStoreUrl = self.driver.current_url
        return GoogleStoreUrl
