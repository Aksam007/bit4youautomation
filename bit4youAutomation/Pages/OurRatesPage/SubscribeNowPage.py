from Locators.LocatorsOfWebsite import locators


class Subscribe(locators):

    def __init__(self, driver):
        self.driver = driver

        self.SubscribeButton = locators.SUBSCRIBE_BUTTON_ON_HOME_PAGE_By_Xpath

    def clickSubscribeButton(self):
        self.driver.find_element_by_xpath(self.SubscribeButton).click()
