from Locators.LocatorsOfWebsite import locators


class AddFundsPage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.AddFundsButton = locators.ADD_FUNDS_BUTTON_BY_XPATH

    def clickAddFundsButton(self):
        self.driver.find_element_by_xpath(self.AddFundsButton).click()
