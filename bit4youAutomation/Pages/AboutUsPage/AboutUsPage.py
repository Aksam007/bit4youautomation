from selenium.webdriver.common.by import By

from Locators.LocatorsOfWebsite import locators
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class AboutUs(locators):

    def __init__(self, driver):
        self.driver = driver

        self.AboutUsButton = locators.ABOUT_US_BUTTON_By_Xpath
        self.AboutUsPageHeading = locators.ABOUT_US_Heading_By_class

    def clickAboutUsButton(self):
        self.driver.find_element_by_xpath(self.AboutUsButton).click()

    def getAboutUsPageHeading(self):
        BigTitle = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, self.AboutUsPageHeading)))
        return BigTitle.text
