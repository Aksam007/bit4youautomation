from Pages.NewBeeProBankPage.NewBeePROBankPage import NewBeeProBank
from TestCases.TestRunnerConfig import TestRunner


class doNewBeePROBankTest(TestRunner):

    def test_NewBeeStartDemo(self):
        driver = self.driver
        NewBeeProBankObj = NewBeeProBank(driver)
        NewBeeProBankObj.clickNewBeeStartDemoButton()

    def test_ProGetStarted(self):
        driver = self.driver
        NewBeeProBankObj = NewBeeProBank(driver)
        NewBeeProBankObj.clickProGetStartedButton()

    def test_BankContactUs(self):
        driver = self.driver
        NewBeeProBankObj = NewBeeProBank(driver)
        NewBeeProBankObj.clickBankContactUsButton()
