import unittest
import os
from HTMLTestRunner import HTMLTestRunner

from AboutUsPageTestCase.AboutUsTestCase import doAboutPageTest
from SupportTestCase.SupportPageTestCase import doSupportTest
from ChangeWebLanguageTestCase.ChangeLanguageTestCase import doChangeWebLanguage
from DiscoverTheAppOnMobileStoresTestCase.DiscoverTheAppTestCase import doDiscoverAppOnStores
from LoginPageTestCase.GetStartedNowTestCase import doGetStartedTest
from LoginPageTestCase.GetFreeDemoAccountTestCase import doFreeAccountDemoTest
from LoginPageTestCase.LoginTestCases import doLoginTest
from NewBeeProBankTestCase.NewBeePROBankTestCases import doNewBeePROBankTest
from OurRatesTestCase.SubscribeTestCases import doSubscribeNowTest
from OurServicesPageTestCases.DemoTestCase import doOurServicesDemoTest
# from TestCases.OurServicesPageTestCases.ExchangeAssetsPageTestCase import doOurServicesExchangeAssetsTest
from OurServicesPageTestCases.BanContactTestCase import doOurServicesBanContactTest
from OurServicesPageTestCases.WalletsAvailableCryptoTestCase import doOurServicesWalletTest
from OurServicesPageTestCases.InStorePaymentPageTestCases import doOurServicesInStorePaymentTest
from OurServicesPageTestCases.FiatWithdrawalTestCase import doOurServicesFiatWithdrawalTest
from OurAssetPageTestCase.OurAssetSectionTest import doOurAssetsTest
from OurAssetPageTestCase.BuyRippleTestCase import doBuyRippleNowTest
from OurAssetPageTestCase.BuyBitcoinTestCase import doBuyBitcoinNowTest
from OurAssetPageTestCase.BuyLiteCoinTestCase import doBuyLiteCoinNowTest
from OurAssetPageTestCase.BuyEthereumTestCase import doBuyEthereumNowTest
from OurAssetPageTestCase.BlockChainRevolutionTestCase import doBlockChainRevolutionTest
# from TestCases.SignUpTestCase.SignUpTestCase import
from VideoOnHomePageTestCase.VideoTestCase import doStartAndStopVideo
from AsSeenInPageTestCase.AsSeenInTest import doAsSeenInPageTest
from BuyYourFirstCryptoTestCases.BuyAndSellFirstCrytpoTestCase import doBuyAndSellFirstCrypto
from BuyYourFirstCryptoTestCases.CreateAnAccountTestCase import doCreateAnAccount
from BuyYourFirstCryptoTestCases.CreateAFreeAccountTestCase import doCreateAFreeAccount
from BuyYourFirstCryptoTestCases.VerifyYourAccountTestCase import doVerifyYourAccount
from FooterPageTestCase.AboutTestCase import doAboutInFooterTest
from FooterPageTestCase.ContactTestCase import doContactInFooterTest
from FooterPageTestCase.MembersTestCase import doMembersInFooterTest
from FooterPageTestCase.ResourcesTestCase import doResourcesInFooterTest
from FooterPageTestCase.SocialMediaLinksTestCase import doSocialMediaLinksInFooterTest
from LogoutTestCase.LogoutTestCase import doLogoutTest
from OpenOrdersTestCase.OpenOrdersTest import doOpenOrderPageTest
from PositionsPageTestCase.PositionsTestCase import doPositionsPageTest
from WalletsPageTestCase.WalletsPageTest import doWalletsPageTest
from HistoryPageTestCase.HistoryTestCase import doHistoryPageTest
from SettingsPageTestCase.SettingsPageTest import doSettingsPageTest


if __name__ == "__main__":
    LoginPage_tests = unittest.TestLoader().loadTestsFromTestCase(doLoginTest)
    AboutPage_tests = unittest.TestLoader().loadTestsFromTestCase(doAboutPageTest)
    SupportPage_tests = unittest.TestLoader().loadTestsFromTestCase(doSupportTest)
    ChangeWebLanguage_tests = unittest.TestLoader().loadTestsFromTestCase(doChangeWebLanguage)
    GetStarted_tests = unittest.TestLoader().loadTestsFromTestCase(doGetStartedTest)
    NewBeePROBank_tests = unittest.TestLoader().loadTestsFromTestCase(doNewBeePROBankTest)
    DiscoverAppOnStores_tests = unittest.TestLoader().loadTestsFromTestCase(doDiscoverAppOnStores)
    FreeAccountDemo_tests = unittest.TestLoader().loadTestsFromTestCase(doFreeAccountDemoTest)
    SubscribeNow_tests = unittest.TestLoader().loadTestsFromTestCase(doSubscribeNowTest)
    OurServicesDemo_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesDemoTest)
    # OurServicesExchangeAssets_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesExchangeAssetsTest)
    OurServicesBanContact_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesBanContactTest)
    OurServicesWallet_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesWalletTest)
    OurServicesInStorePayment_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesInStorePaymentTest)
    OurServicesFiatWithdrawal_tests = unittest.TestLoader().loadTestsFromTestCase(doOurServicesFiatWithdrawalTest)
    StartAndStopVideo_tests = unittest.TestLoader().loadTestsFromTestCase(doStartAndStopVideo)
    OurAssets_tests = unittest.TestLoader().loadTestsFromTestCase(doOurAssetsTest)
    BuyRippleNow_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyRippleNowTest)
    BuyBitcoinNow_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyBitcoinNowTest)
    BuyLiteCoinNow_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyLiteCoinNowTest)
    BuyEthereumNow_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyEthereumNowTest)
    BlockChainRevolution_tests = unittest.TestLoader().loadTestsFromTestCase(doBlockChainRevolutionTest)
    AsSeenInPage_tests = unittest.TestLoader().loadTestsFromTestCase(doAsSeenInPageTest)
    BuyAndSellFirstCrypto_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyAndSellFirstCrypto)
    CreateAnAccount_tests = unittest.TestLoader().loadTestsFromTestCase(doCreateAnAccount)
    CreateAFreeAccount_tests = unittest.TestLoader().loadTestsFromTestCase(doCreateAFreeAccount)
    VerifyYourAccount_tests = unittest.TestLoader().loadTestsFromTestCase(doVerifyYourAccount)
    AboutInFooterTest_tests = unittest.TestLoader().loadTestsFromTestCase(doAboutInFooterTest)
    ContactInFooterTest_tests = unittest.TestLoader().loadTestsFromTestCase(doContactInFooterTest)
    MembersInFooter_tests = unittest.TestLoader().loadTestsFromTestCase(doMembersInFooterTest)
    ResourcesInFooter_tests = unittest.TestLoader().loadTestsFromTestCase(doResourcesInFooterTest)
    SocialMediaLinksInFooter_tests = unittest.TestLoader().loadTestsFromTestCase(doSocialMediaLinksInFooterTest)
    LogoutPage_tests = unittest.TestLoader().loadTestsFromTestCase(doLogoutTest)
    OpenOrderPage_tests = unittest.TestLoader().loadTestsFromTestCase(doOpenOrderPageTest)
    PositionsPage_tests = unittest.TestLoader().loadTestsFromTestCase(doPositionsPageTest)
    WalletsPage_tests = unittest.TestLoader().loadTestsFromTestCase(doWalletsPageTest)
    HistoryPage_tests = unittest.TestLoader().loadTestsFromTestCase(doHistoryPageTest)
    SettingsPage_tests = unittest.TestLoader().loadTestsFromTestCase(doSettingsPageTest)

    test_suite = unittest.TestSuite([LoginPage_tests, AboutPage_tests, SupportPage_tests, ChangeWebLanguage_tests,
                                     GetStarted_tests, LogoutPage_tests, OpenOrderPage_tests, PositionsPage_tests,
                                     NewBeePROBank_tests, DiscoverAppOnStores_tests, FreeAccountDemo_tests,
                                     SubscribeNow_tests, OurServicesDemo_tests,
                                     OurServicesBanContact_tests, OurServicesWallet_tests,
                                     OurServicesInStorePayment_tests, OurServicesFiatWithdrawal_tests,
                                     StartAndStopVideo_tests, OurAssets_tests, BuyRippleNow_tests, BuyBitcoinNow_tests,
                                     BuyLiteCoinNow_tests, BuyEthereumNow_tests, BlockChainRevolution_tests,
                                     AsSeenInPage_tests, BuyAndSellFirstCrypto_tests, CreateAnAccount_tests,
                                     CreateAFreeAccount_tests, VerifyYourAccount_tests, AboutInFooterTest_tests,
                                     ContactInFooterTest_tests, MembersInFooter_tests, ResourcesInFooter_tests,
                                     SocialMediaLinksInFooter_tests, WalletsPage_tests, SettingsPage_tests,
                                     HistoryPage_tests])

    currentDir = os.getcwd()
    outfile = open(currentDir + "\Bit4youAutomationReport.html", "wb")
    runner = HTMLTestRunner(
        stream=outfile,
        title='Bit4you Automation Report',
        description='Bit4you Automation Tests'
    )
    runner.run(test_suite)
