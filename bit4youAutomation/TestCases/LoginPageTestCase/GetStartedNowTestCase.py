from Pages.LoginPage.GetStartedNow import GetStartedNow
from TestCases.TestRunnerConfig import TestRunner


class doGetStartedTest(TestRunner):

    def test_GetStartedNow(self):
        driver = self.driver

        GetStartedNowObj = GetStartedNow(driver)
        GetStartedNowObj.clickGetStartedNowButton()
