from Pages.LoginPage.GetFreeDemoAccount import GetFreeDemoAccount
from TestCases.TestRunnerConfig import TestRunner


class doFreeAccountDemoTest(TestRunner):

    def test_GetFreeDemoAccount(self):
        driver = self.driver

        GetFreeDemoAccountObj = GetFreeDemoAccount(driver)
        GetFreeDemoAccountObj.clickGetFreeDemoAccountButton()
