import time

from Pages.LoginPage.LoginPage import Login
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doLoginTest(TestRunner):

    def test_Login(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()