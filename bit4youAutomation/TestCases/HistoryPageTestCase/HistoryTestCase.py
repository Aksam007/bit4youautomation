import time
from Pages.HistoryPage.History import HistoryPage
from Pages.LoginPage.LoginPage import Login
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doHistoryPageTest(TestRunner):

    def test_PositionsHistory(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        HistoryPageObj = HistoryPage(driver)
        HistoryPageObj.hoverOnHistoryButton()
        HistoryPageObj.clickOnPositionsHistory()

    def test_OrdersHistory(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        HistoryPageObj = HistoryPage(driver)
        HistoryPageObj.hoverOnHistoryButton()
        HistoryPageObj.clickOnOrdersHistory()

    def test_FundsHistory(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        HistoryPageObj = HistoryPage(driver)
        HistoryPageObj.hoverOnHistoryButton()
        HistoryPageObj.clickOnFundsHistory()

    def test_BlockchainHistory(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        HistoryPageObj = HistoryPage(driver)
        HistoryPageObj.hoverOnHistoryButton()
        HistoryPageObj.clickOnBlockchainHistory()

    def test_ExportHistory(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        HistoryPageObj = HistoryPage(driver)
        HistoryPageObj.hoverOnHistoryButton()
        HistoryPageObj.clickOnExportHistory()