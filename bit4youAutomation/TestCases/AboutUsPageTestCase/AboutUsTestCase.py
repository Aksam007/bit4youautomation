from Pages.AboutUsPage.AboutUsPage import AboutUs
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doAboutPageTest(TestRunner):

    def test_AboutUs(self):
        driver = self.driver

        AboutUsObj = AboutUs(driver)
        AboutUsObj.clickAboutUsButton()
        EUExchangeHeading = AboutUsObj.getAboutUsPageHeading()
        assert EUExchangeHeading == TestData.AboutUsPageTitle
