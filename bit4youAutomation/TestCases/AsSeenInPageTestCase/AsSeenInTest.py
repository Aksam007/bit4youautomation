from Pages.AsSeenInPage.AsSeenIn import AsSeenInArticles
from TestCases.TestRunnerConfig import TestRunner
from ConfigTestData.TestData import TestData


class doAsSeenInPageTest(TestRunner):

    def test_AsSeenIn_SevenSur(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clickSevenSurButton()
        SevenSurWebpage = self.driver.current_url
        assert SevenSurWebpage == TestData.SevenSurWebPageLink

    def test_AsSeenIn_TrendsTendance(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clickTrendsTendanceButton()

    def test_AsSeenIn_CanalZ(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clickCanalZButton()

    def test_AsSeenIn_LeSoir(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clickLeSoirButton()

    def test_AsSeenIn_LaLibre(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clicklaLibreButton()

    def test_AsSeenIn_LN24(self):
        driver = self.driver
        AsSeenInArticlesObj = AsSeenInArticles(driver)
        AsSeenInArticlesObj.clickLN24Button()
