from Pages.DiscoverTheAppOnMobileStoresPage.DiscoverTheAppPage import DiscoverTheAppOnStores
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doDiscoverAppOnStores(TestRunner):

    def test_DiscoverAppOnAppleStore(self):
        driver = self.driver

        DiscoverTheAppOnStoresObj = DiscoverTheAppOnStores(driver)
        AppleStoreURL = DiscoverTheAppOnStoresObj.clickAppleStoreButton()
        assert AppleStoreURL == TestData.DiscoverTheAppOnAppleStore

    def test_DiscoverAppOnGoogleStore(self):
        driver = self.driver

        DiscoverTheAppOnStoresObj = DiscoverTheAppOnStores(driver)
        GoogleStoreURL = DiscoverTheAppOnStoresObj.clickGoogleStoreButton()
        assert GoogleStoreURL == TestData.DiscoverTheAppOnGoogleStore
