import time
from Pages.SettingsPage.Settings import SettingsPage
from Pages.LoginPage.LoginPage import Login
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doSettingsPageTest(TestRunner):

    def test_ChangeCurrency(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        SettingsPageObj = SettingsPage(driver)
        SettingsPageObj.clickSettingsButton()
        SettingsPageObj.clickCurrencyDropdown()
        SettingsPageObj.selectEuro()
        Currency = SettingsPageObj.getCurrencyOnDashboard()
        assert Currency == TestData.EuroCurrency

    def test_ChangeLanguageToNederlands(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        SettingsPageObj = SettingsPage(driver)
        SettingsPageObj.clickSettingsButton()
        SettingsPageObj.clickLanguageDropdown()
        SettingsPageObj.selectNederlands()
        Language = SettingsPageObj.getLanguageOnDashboard()
        assert Language == TestData.NederlandsLanguage

    def test_ChangeLanguageToEnglish(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        SettingsPageObj = SettingsPage(driver)
        SettingsPageObj.clickSettingsButton()
        SettingsPageObj.clickLanguageDropdown()
        SettingsPageObj.selectEnglish()

    def test_Referrals(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        SettingsPageObj = SettingsPage(driver)
        SettingsPageObj.clickSettingsButton()
        SettingsPageObj.clickReferralsButton()
        SettingsPageObj.setReferralAmount(TestData.ReferralAmount)
        SettingsPageObj.clickSaveButton()

    def test_SignOut(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        SettingsPageObj = SettingsPage(driver)
        SettingsPageObj.clickSettingsButton()
        SettingsPageObj.clickSignOutButton()
