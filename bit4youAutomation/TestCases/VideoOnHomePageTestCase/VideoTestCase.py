import time

from Pages.VideoOnHomePage.VideoHomePage import Video
from TestCases.TestRunnerConfig import TestRunner


class doStartAndStopVideo(TestRunner):

    def test_StartAndStopVideo(self):
        driver = self.driver

        VideoObj = Video(driver)
        VideoObj.clickVideoFrame()
        time.sleep(1)
        VideoObj.clickVideoFrame()
