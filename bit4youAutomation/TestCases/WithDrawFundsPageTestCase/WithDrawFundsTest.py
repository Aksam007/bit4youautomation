from datetime import time

from ConfigTestData.TestData import TestData
from Pages.LoginPage.LoginPage import Login
from TestCases.TestRunnerConfig import TestRunner
from Pages.WithDrawFundsPage.WithdrawFunds import WithdrawFundsPage


class doWithdrawFundsPageTest(TestRunner):

    def test_clickWithdrawFunds(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        WithdrawFundsPageObj = WithdrawFundsPage(driver)
        WithdrawFundsPageObj.clickWithdrawFundsButton()