import time

from ConfigTestData.TestData import TestData
from Pages.LoginPage.LoginPage import Login
from TestCases.TestRunnerConfig import TestRunner
from Pages.Walletspage.WalletsPage import WalletsPage


class doWalletsPageTest(TestRunner):

    def test_ReceiveAssets(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        WalletsPageObj = WalletsPage(driver)
        WalletsPageObj.clickWalletsButton()
        time.sleep(10)
        WalletsPageObj.clickBitCoinCashBox()
        time.sleep(5)
        WalletsPageObj.clickReceiveAssetsButton()

    def test_SendAssets(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        WalletsPageObj = WalletsPage(driver)
        WalletsPageObj.clickWalletsButton()
        time.sleep(10)
        WalletsPageObj.clickBitCoinCashBox()
        time.sleep(5)
        WalletsPageObj.clickSendAssetsButton()

    def test_BuyBitcoinCash(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        WalletsPageObj = WalletsPage(driver)
        WalletsPageObj.clickWalletsButton()
        time.sleep(10)
        WalletsPageObj.clickBitCoinCashBox()
        time.sleep(5)
        WalletsPageObj.clickBuyBitcoinCashButton()

    def test_SellBitcoinCash(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        WalletsPageObj = WalletsPage(driver)
        WalletsPageObj.clickWalletsButton()
        time.sleep(10)
        WalletsPageObj.clickBitCoinCashBox()
        time.sleep(5)
        WalletsPageObj.clickSellBitcoinCashButton()

    def test_getTransactionID(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        WalletsPageObj = WalletsPage(driver)
        WalletsPageObj.clickWalletsButton()
        time.sleep(10)
        WalletsPageObj.clickBitCoinCashBox()
        time.sleep(10)
        WalletsPageObj.clickTransactionBox()
        time.sleep(10)
        TransID = WalletsPageObj.getTransactionIdFromFirstTransaction()
        assert TransID == TestData.Wallets_First_Transaction_ID_BitcoinCash
