import time

from ConfigTestData.TestData import TestData
from Pages.LoginPage.LoginPage import Login
from TestCases.TestRunnerConfig import TestRunner
from Pages.PositionsPage.Positions import PositionsPage
from Locators.LocatorsOfWebsite import locators


class doPositionsPageTest(TestRunner):

    def test_applyStopLoss(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        PositionsPageObj = PositionsPage(driver)
        time.sleep(15)
        PositionsPageObj.clickPositionsButton()
        time.sleep(10)
        PositionsPageObj.clickFirstPosition()
        time.sleep(5)
        PositionsPageObj.clickStopLossButton()
        # checkboxValue = self.driver.find_element_by_xpath("//input[@aria-label='Enable Stop Loss?']")
        # print(checkboxValue.is_selected())
        # if checkboxValue.is_selected():
        #     PositionsPageObj.clickFollowingStopLossCheckBox()
        #     time.sleep(15)
        #     PositionsPageObj.clearStopLossField()
        #     time.sleep(15)
        #     PositionsPageObj.clickEnableStopLossButton()
        #     time.sleep(15)
        #     PositionsPageObj.clickSaveButton()
        # else:
        PositionsPageObj.clickEnableStopLossButton()
        time.sleep(15)
        PositionsPageObj.clearStopLossField()
        time.sleep(15)
        PositionsPageObj.enterDataInStopLossField(TestData.StopLossPercentage)
        time.sleep(15)
        PositionsPageObj.clickFollowingStopLossCheckBox()
        time.sleep(15)
        PositionsPageObj.clickSaveButton()
