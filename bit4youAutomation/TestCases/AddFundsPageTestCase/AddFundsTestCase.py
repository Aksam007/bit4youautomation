import time

from ConfigTestData.TestData import TestData
from Pages.LoginPage.LoginPage import Login
from TestCases.TestRunnerConfig import TestRunner
from Pages.AddFundsPage.AddFund import AddFundsPage


class doAddFundsPageTest(TestRunner):

    def test_clickAddFunds(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        AddFundsPageObj = AddFundsPage(driver)
        AddFundsPageObj.clickAddFundsButton()