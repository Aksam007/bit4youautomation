import time

from Pages.OurAssetsPage.BuyBitcoin import BitcoinButtonOnOurAsset
from TestCases.TestRunnerConfig import TestRunner


class doBuyBitcoinNowTest(TestRunner):

    def test_OurAssetBitcoinButton(self):
        driver = self.driver

        BitcoinButtonOnOurAssetObj = BitcoinButtonOnOurAsset(driver)
        time.sleep(5)
        BitcoinButtonOnOurAssetObj.clickBitcoinButton()
        time.sleep(5)
        BitcoinButtonOnOurAssetObj.clickBuyBitcoinNowButton()
