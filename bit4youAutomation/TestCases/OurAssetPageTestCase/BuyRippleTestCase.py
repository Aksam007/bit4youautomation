import time

from Pages.OurAssetsPage.BuyRipple import RippleButtonOnOurAsset
from TestCases.TestRunnerConfig import TestRunner


class doBuyRippleNowTest(TestRunner):

    def test_OurAssetRippleButton(self):
        driver = self.driver

        RippleButtonOnOurAssetObj = RippleButtonOnOurAsset(driver)
        time.sleep(5)
        RippleButtonOnOurAssetObj.clickRippleButton()
        time.sleep(5)
        RippleButtonOnOurAssetObj.clickBuyRippleCoinNowButton()
