import time

from Pages.OurAssetsPage.BuyLiteCoin import LiteCoinButtonOnOurAsset
from TestCases.TestRunnerConfig import TestRunner


class doBuyLiteCoinNowTest(TestRunner):

    def test_OurAssetLiteCoinButton(self):
        driver = self.driver

        LiteCoinButtonOnOurAssetObj = LiteCoinButtonOnOurAsset(driver)
        time.sleep(5)
        LiteCoinButtonOnOurAssetObj.clickLiteCoinButton()
        time.sleep(5)
        LiteCoinButtonOnOurAssetObj.clickBuyLiteCoinNowButton()
