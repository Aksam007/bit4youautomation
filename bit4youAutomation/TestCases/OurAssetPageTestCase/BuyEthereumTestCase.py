import time

from Pages.OurAssetsPage.BuyEthereum import EthereumButtonOnOurAsset
from TestCases.TestRunnerConfig import TestRunner


class doBuyEthereumNowTest(TestRunner):

    def test_OurAssetEthereumButton(self):
        driver = self.driver

        EthereumButtonOnOurAssetObj = EthereumButtonOnOurAsset(driver)
        time.sleep(5)
        EthereumButtonOnOurAssetObj.clickEthereumButton()
        time.sleep(5)
        EthereumButtonOnOurAssetObj.clickBuyEthereumNowButton()
