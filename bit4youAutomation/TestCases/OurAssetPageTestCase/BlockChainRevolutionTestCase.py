import time

from Pages.OurAssetsPage.BlockChainRevolution import BlockChainRevolution
from TestCases.TestRunnerConfig import TestRunner
from ConfigTestData.TestData import TestData


class doBlockChainRevolutionTest(TestRunner):

    def test_BlockChainRevolution(self):
        driver = self.driver

        BlockChainRevolutionObj = BlockChainRevolution(driver)
        BlockChainRevolutionObj.clickBitcoinButton()
        BlockChainRevolutionObj.clickBlockChainRevolutionButton()
        BlockChainMainHeadingText = BlockChainRevolutionObj.getBlockChainRevolutionMainHeading()
        assert BlockChainMainHeadingText == TestData.BlockChainRevolutionMainHeading
