import time

from Pages.OurAssetsPage.OurAssetButton import OurAssetSectionAllButtons
from TestCases.TestRunnerConfig import TestRunner


class doOurAssetsTest(TestRunner):

    def test_OurAssetAllButtons(self):
        driver = self.driver

        OurAssetSectionAllButtonsObj = OurAssetSectionAllButtons(driver)
        time.sleep(5)
        OurAssetSectionAllButtonsObj.clickAllOurAssetsButton()
