import time

from Pages.ChangeWebLanguage.ChangeWebLanguagePage import ChangeWebLanguage
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doChangeWebLanguage(TestRunner):

    def test_Francias_Language(self):
        driver = self.driver

        ChangeWebLanguageObj = ChangeWebLanguage(driver)
        ChangeWebLanguageObj.clickChangeLanguage()
        ChangeWebLanguageObj.selectFranciasLanguage()
        FranciasTextOnHomePage = ChangeWebLanguageObj.getFranciasText()
        assert FranciasTextOnHomePage == TestData.FranciasText
        ChangeWebLanguageObj.clickFranciasChangeLanguage()
        ChangeWebLanguageObj.selectEnglishLanguage()
        EnglishURL = ChangeWebLanguageObj.getEnglishBasedWebsiteURL()
        assert EnglishURL == TestData.EnglishTextURL

    def test_Nederland_Language(self):
        driver = self.driver

        ChangeWebLanguageObj = ChangeWebLanguage(driver)
        ChangeWebLanguageObj.clickChangeLanguage()
        ChangeWebLanguageObj.selectNederlandLanguage()
        time.sleep(5)
        NederlandTextOnHomePage = ChangeWebLanguageObj.getNederlandText()
        time.sleep(5)
        assert NederlandTextOnHomePage == TestData.NederlandText
