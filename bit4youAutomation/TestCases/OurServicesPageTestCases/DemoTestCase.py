from Pages.OurServicesPage.DemoPage import OurServicesDemo
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesDemoTest(TestRunner):

    def test_OurServices_Demo(self):
        driver = self.driver

        OurServicesDemoObj = OurServicesDemo(driver)
        OurServicesDemoObj.clickCreateAnAccountButton()
