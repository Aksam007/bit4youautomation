import time

from Pages.OurServicesPage.ExchangeAssetsPage import OurServicesExchangeAssets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesExchangeAssetsTest(TestRunner):

    def test_OurServices_ExchangeAssets(self):
        driver = self.driver

        OurServicesExchangeAssetsObj = OurServicesExchangeAssets(driver)
        OurServicesExchangeAssetsObj.clickExchangeAssetsButton()
        time.sleep(5)
        OurServicesExchangeAssetsObj.EnterAmount(TestData.ExchangeAssetsAmount)
        time.sleep(5)
        OurServicesExchangeAssetsObj.SelectValueFrom_FromDropDown()
        time.sleep(5)
        OurServicesExchangeAssetsObj.SelectValueFrom_ToDropDown()
        time.sleep(5)
