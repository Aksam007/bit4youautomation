from Pages.OurServicesPage.WalletsPage import OurServicesWallets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesWalletTest(TestRunner):

    def test_OurServices_Wallets(self):
        driver = self.driver

        OurServicesWalletsObj = OurServicesWallets(driver)
        OurServicesWalletsObj.clickAvailableCryptoButton()
