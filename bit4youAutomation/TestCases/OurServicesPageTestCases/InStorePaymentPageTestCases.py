import time

from Pages.OurServicesPage.InStorePaymentPage import OurServicesInStorePayment
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesInStorePaymentTest(TestRunner):

    def test_OurServices_InStorePayment(self):
        driver = self.driver

        OurServicesInStorePaymentObj = OurServicesInStorePayment(driver)
        time.sleep(5)
        OurServicesInStorePaymentObj.clickViewMerchantToolsButton()
        MerchantToolHeading = OurServicesInStorePaymentObj.getMerchantToolHeading()
        assert MerchantToolHeading == TestData.MerchantToolMainHeading
        OurServicesInStorePaymentObj.clickGetInTouchButton()
        ContactUsForm = OurServicesInStorePaymentObj.getContactUsFormHeading()
        assert ContactUsForm == TestData.ContactUsFormHeading
