import time

from Pages.OurServicesPage.BanContactpage import OurServicesBanContact
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesBanContactTest(TestRunner):

    def test_OurServices_BanContact(self):
        driver = self.driver

        OurServicesBanContactObj = OurServicesBanContact(driver)
        OurServicesBanContactObj.clickWhatIsUSTDButton()
        WhatIsUSTDMainHeading = OurServicesBanContactObj.getUSTDMainHeading()
        assert WhatIsUSTDMainHeading == TestData.BanContact_USTD_Main_Heading
        OurServicesBanContactObj.clickReadWhitePaperButton()
