
from Pages.OurServicesPage.FiatWithDrawalPage import OurServicesFiatWithdrawal
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doOurServicesFiatWithdrawalTest(TestRunner):

    def test_OurServices_FiatWithdrawal(self):
        driver = self.driver

        OurServicesFiatWithdrawalObj = OurServicesFiatWithdrawal(driver)
        OurServicesFiatWithdrawalObj.clickFiatWithdrawalButton()
        DepositFee = OurServicesFiatWithdrawalObj.getDepositFee()
        assert DepositFee == TestData.FiatWithdrawalDepositFee
        WithdrawalFee = OurServicesFiatWithdrawalObj.getWithdrawalFee()
        assert WithdrawalFee == TestData.FiatWithdrawalWithdrawalFee
        CryptoToCryptoExchange = OurServicesFiatWithdrawalObj.getCrypto_to_CryptoExchangeFee()
        assert CryptoToCryptoExchange == TestData.FiatWithdrawalCrypto_to_cryptoFee
