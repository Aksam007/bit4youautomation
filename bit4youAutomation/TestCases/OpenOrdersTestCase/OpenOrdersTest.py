import time

from ConfigTestData.TestData import TestData
from Pages.LoginPage.LoginPage import Login
from TestCases.TestRunnerConfig import TestRunner
from Pages.OpenOrdersPage.OpenOrders import OpenOrderPage


class doOpenOrderPageTest(TestRunner):

    def test_cancelOpenOrder(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        OpenOrderPageObj = OpenOrderPage(driver)
        OpenOrderPageObj.clickOpenOrdersButton()
        time.sleep(5)
        OpenOrderPageObj.cancelFirstOrder()
        time.sleep(15)