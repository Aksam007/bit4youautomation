import time

from Pages.NewsPage.News import News
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doNewsPageTest(TestRunner):

    def test_NewsPage(self):
        driver = self.driver

        NewsObj = News(driver)
        NewsObj.clickNewsButton()
        CryptoNewsHeading = NewsObj.getNewsPageHeading()
        assert CryptoNewsHeading == TestData.NewsPageMainHeading
        NewsObj.clickReadMoreButton()
