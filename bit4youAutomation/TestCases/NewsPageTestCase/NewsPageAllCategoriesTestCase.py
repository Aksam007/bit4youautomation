import time

from Pages.NewsPage.NewsPageAllCategories import NewsPageAllCategories
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doNewsPageAllCategoriesTest(TestRunner):

    def test_NewsPageAllCategories(self):
        driver = self.driver

        NewsPageAllCategoriesObj = NewsPageAllCategories(driver)
        NewsPageAllCategoriesObj.clickNewsButton()
        time.sleep(10)
        NewsPageAllCategoriesObj.clickAllCategoriesButton()
