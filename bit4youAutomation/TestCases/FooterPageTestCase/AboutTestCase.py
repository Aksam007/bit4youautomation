from Pages.FooterPage.About import AboutInFooter
from TestCases.TestRunnerConfig import TestRunner


class doAboutInFooterTest(TestRunner):

    def test_About_AboutUsButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickAboutUsButtonInFooter()

    def test_About_TermsAndConditionButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickTermsAndConditionButtonInFooter()

    def test_About_CookiePolicyButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickCookiePolicyButtonInFooter()

    def test_About_PrivacyPolicyButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickPrivacyPolicyButtonInFooter()

    def test_About_ConsumerClaimsButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickConsumerClaimsButtonInFooter()

    def test_About_SystemStatusButton(self):
        driver = self.driver
        AboutInFooterObj = AboutInFooter(driver)
        AboutInFooterObj.clickSystemStatusButtonInFooter()