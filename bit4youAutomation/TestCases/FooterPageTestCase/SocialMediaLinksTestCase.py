from Pages.FooterPage.SocialMediaLinks import SocialMediaButtonsInFooter
from TestCases.TestRunnerConfig import TestRunner


class doSocialMediaLinksInFooterTest(TestRunner):

    def test_SocialMediaLink_Twitter(self):
        driver = self.driver
        SocialMediaButtonsInFooterObj = SocialMediaButtonsInFooter(driver)
        SocialMediaButtonsInFooterObj.clickTwitterButtonInFooter()

    def test_SocialMediaLink_Facebook(self):
        driver = self.driver
        SocialMediaButtonsInFooterObj = SocialMediaButtonsInFooter(driver)
        SocialMediaButtonsInFooterObj.clickFacebookButtonInFooter()

    def test_SocialMediaLink_Instagram(self):
        driver = self.driver
        SocialMediaButtonsInFooterObj = SocialMediaButtonsInFooter(driver)
        SocialMediaButtonsInFooterObj.clickInstagramButtonInFooter()

    def test_SocialMediaLink_Gitlab(self):
        driver = self.driver
        SocialMediaButtonsInFooterObj = SocialMediaButtonsInFooter(driver)
        SocialMediaButtonsInFooterObj.clickGitlabButtonInFooter()
