from Pages.FooterPage.Members import MembersInFooter
from TestCases.TestRunnerConfig import TestRunner


class doMembersInFooterTest(TestRunner):

    def test_Members_SignUpButton(self):
        driver = self.driver
        MembersInFooterObj = MembersInFooter(driver)
        MembersInFooterObj.clickSignUpButtonInFooter()

    def test_Members_LoginButton(self):
        driver = self.driver
        MembersInFooterObj = MembersInFooter(driver)
        MembersInFooterObj.clickLoginButtonInFooter()

    def test_Members_LostPasswordButton(self):
        driver = self.driver
        MembersInFooterObj = MembersInFooter(driver)
        MembersInFooterObj.clickLostPasswordButtonInFooter()

    def test_Members_ServicesAndFeesButton(self):
        driver = self.driver
        MembersInFooterObj = MembersInFooter(driver)
        MembersInFooterObj.clickServicesAndFeeButtonInFooter()