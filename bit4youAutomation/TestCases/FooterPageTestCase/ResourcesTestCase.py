from Pages.FooterPage.Resources import ResourcesInFooter
from TestCases.TestRunnerConfig import TestRunner


class doResourcesInFooterTest(TestRunner):

    def test_Resources_HomeButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickHomeButtonInFooter()

    def test_Resources_NewsButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickNewsButtonInFooter()

    def test_Resources_SupportButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickSupportButtonInFooter()

    def test_Resources_APIDocsButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickAPIDocsButtonInFooter()

    def test_Resources_CurrencyConverterButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickCurrencyConverterInFooter()

    def test_Resources_WhoisIPButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickWhoisIpInFooter()

    def test_Resources_OurMarketsButton(self):
        driver = self.driver
        ResourcesInFooterObj = ResourcesInFooter(driver)
        ResourcesInFooterObj.clickOurMarketsButtonInFooter()