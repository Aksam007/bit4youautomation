import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from ConfigTestData.TestData import TestData


class TestRunner(unittest.TestCase):

    def setUp(self):
        options = Options()
        options.headless = False
        options.add_argument("window-size=1400,600")
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        self.driver.get(TestData.BASE_URL)
        self.driver.maximize_window()
        self.driver.find_element_by_xpath("//button[@aria-label='Close']").click()
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.driver.quit()
