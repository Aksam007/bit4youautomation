import time

from Pages.SupportPage.SupportPage import Support
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doSupportTest(TestRunner):

    def test_Ask_A_Question(self):
        driver = self.driver
        SupportPageObj = Support(driver)
        SupportPageObj.clickSupportButton()
        time.sleep(5)
        SupportPageObj.AskAQuestion(TestData.AskAQuestionText)
        time.sleep(5)
        SupportPageObj.clickAppearedSuggestionInAskAQuestion()

    def test_Suggested_Articles(self):
        driver = self.driver
        SupportPageObj = Support(driver)
        SupportPageObj.clickSupportButton()
        time.sleep(5)
        SupportPageObj.clickCryptoAssetSuggestedRead()
        time.sleep(5)
        MainTitle = SupportPageObj.getCryptoAssetMainTitle()
        assert MainTitle == TestData.SuggestedReadCryptoAsset
