import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.USDTMarkets import USTDMarkets
from Pages.MarketsPage.SellBitcoinFromMarket import SellBitcoinFromUSTDMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doSellBitcoinFromUSDTMarket(TestRunner):

    def test_SellBitcoinOnMarketRate(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        USTDMarketsObj = USTDMarkets(driver)
        USTDMarketsObj.clickUSDTMarketButton()
        SellBitcoinFromUSTDMarketsObj = SellBitcoinFromUSTDMarkets(driver)
        SellBitcoinFromUSTDMarketsObj.SellBitcoinInUSDTMarketOnMarketRate(TestData.SellingAmount)

    def test_SellBitcoinOnCustomRate(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        USTDMarketsObj = USTDMarkets(driver)
        USTDMarketsObj.clickUSDTMarketButton()
        time.sleep(10)
        SellBitcoinFromUSTDMarketsObj = SellBitcoinFromUSTDMarkets(driver)
        time.sleep(10)
        SellBitcoinFromUSTDMarketsObj.SellBitcoinInUSDTMarketOnCustomRate(TestData.SellingAmount, TestData.SellingCustomRate)