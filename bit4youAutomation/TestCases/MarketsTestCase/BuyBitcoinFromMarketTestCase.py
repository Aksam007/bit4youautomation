import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.USDTMarkets import USTDMarkets
from Pages.MarketsPage.BuyBitcoinFromMarket import BuyBitcoinFromUSTDMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doBuyBitcoinFromUSDTMarket(TestRunner):

    def test_buyBitcoinOnMarketRate(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        USTDMarketsObj = USTDMarkets(driver)
        USTDMarketsObj.clickUSDTMarketButton()
        BuyBitcoinFromUSTDMarketsObj = BuyBitcoinFromUSTDMarkets(driver)
        BuyBitcoinFromUSTDMarketsObj.buyBitcoinOnUSDTMarketOnMarketRate(TestData.Amount)

    def test_buyBitcoinOnCustomRate(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        USTDMarketsObj = USTDMarkets(driver)
        USTDMarketsObj.clickUSDTMarketButton()
        time.sleep(10)
        BuyBitcoinFromUSTDMarketsObj = BuyBitcoinFromUSTDMarkets(driver)
        time.sleep(10)
        BuyBitcoinFromUSTDMarketsObj.buyBitcoinOnUSDTMarketOnCustomRate(TestData.BuyingAmount, TestData.BuyingCustomRate)