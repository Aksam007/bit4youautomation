import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.Favourites import FavouriteMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doFavouriteMarket(TestRunner):

    def test_AddFavouriteMarket(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(10)
        FavouriteMarketsObj = FavouriteMarkets(driver)
        time.sleep(5)
        FavouriteMarketsObj.clickFavourites()
        FavouriteMarketsObj.clickAddMarket()
        FavouriteMarketsObj.clickAddFavouriteMarketDropDown()
        time.sleep(10)
        FavouriteMarketsObj.clickUnfavouriteButton()
        time.sleep(5)