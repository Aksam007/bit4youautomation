import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.ETHMarket import ETHMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doETHMarket(TestRunner):

    def test_clickAllETHMarkets(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(5)
        ETHMarketsObj = ETHMarkets(driver)
        ETHMarketsObj.clickETHMarketButton()
        ETHMarketsObj.clickOnAllETHMarkets()