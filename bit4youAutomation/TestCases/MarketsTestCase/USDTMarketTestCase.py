import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.Favourites import FavouriteMarkets
from Pages.MarketsPage.USDTMarkets import USTDMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doUSDTMarket(TestRunner):

    def test_AddFavouriteMarketUponClickingStar(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(10)
        USDTMarketsObj = USTDMarkets(driver)
        USDTMarketsObj.clickUSDTMarketButton()
        USDTMarketsObj.clickOnFavouriteStartButton()
        FavouriteMarketsObj = FavouriteMarkets(driver)
        time.sleep(5)
        FavouriteMarketsObj.clickFavourites()
        time.sleep(5)
        FavouriteMarketsObj.clickUnfavouriteButton()
        time.sleep(5)

    def test_clickAllUSDTMarkets(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(10)
        USDTMarketsObj = USTDMarkets(driver)
        USDTMarketsObj.clickUSDTMarketButton()
        USDTMarketsObj.clickOnAllUSDTMarkets()