import time

from Pages.LoginPage.LoginPage import Login
from Pages.MarketsPage.BTCMarket import BTCMarkets
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doBTCMarket(TestRunner):

    def test_clickAllBTCMarkets(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(10)
        BTCMarketsObj = BTCMarkets(driver)
        BTCMarketsObj.clickBTCMarketButton()
        BTCMarketsObj.clickOnAllBTCMarkets()