from Pages.BuyYourFirstCryptoPage.CreateAnAccount import BuyAndSellCryptoPageCreateAnAccount
from TestCases.TestRunnerConfig import TestRunner


class doCreateAnAccount(TestRunner):

    def test_CreateAnAccount(self):
        driver = self.driver

        BuyAndSellCryptoPageCreateAnAccountObj = BuyAndSellCryptoPageCreateAnAccount(driver)
        BuyAndSellCryptoPageCreateAnAccountObj.clickBuyAndSellCryptoCreateAnAccountButton()
