from Pages.BuyYourFirstCryptoPage.CreateAFreeAccount import BuyAndSellCryptoPageCreateAFreeAccount
from TestCases.TestRunnerConfig import TestRunner


class doCreateAFreeAccount(TestRunner):

    def test_CreateAFreeAccount(self):
        driver = self.driver

        BuyAndSellCryptoPageCreateAFreeAccountObj = BuyAndSellCryptoPageCreateAFreeAccount(driver)
        BuyAndSellCryptoPageCreateAFreeAccountObj.clickBuyAndSellCryptoCreateAFreeAccountButton()
