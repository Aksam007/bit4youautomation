from Pages.BuyYourFirstCryptoPage.VerifyYourAccount import BuyAndSellCryptoAssetsVerifyYourAccount
from TestCases.TestRunnerConfig import TestRunner


class doVerifyYourAccount(TestRunner):

    def test_VerifyYourAccount(self):
        driver = self.driver

        BuyAndSellCryptoAssetsVerifyYourAccountObj = BuyAndSellCryptoAssetsVerifyYourAccount(driver)
        BuyAndSellCryptoAssetsVerifyYourAccountObj.clickVerifyYourAccountButton()
