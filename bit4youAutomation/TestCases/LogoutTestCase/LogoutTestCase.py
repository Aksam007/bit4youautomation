import time

from Pages.LoginPage.LoginPage import Login
from Pages.LogoutPage.Logout import Logout
from ConfigTestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doLogoutTest(TestRunner):

    def test_Logout(self):
        driver = self.driver

        LoginObj = Login(driver)
        LoginObj.clickLoginButtonOnHomepage()
        LoginObj.loginUserEmailOrPhone(TestData.PhoneNumberOrEmail)
        LoginObj.loginPassword(TestData.Pin)
        LoginObj.clickLoginButtonOnLoginPage()
        time.sleep(15)
        LoginObj.getOtpMessage()
        LoginObj.clickContinueButton()
        time.sleep(3)
        LogoutObj = Logout(driver)
        LogoutObj.clickLogoutButton()
        time.sleep(10)