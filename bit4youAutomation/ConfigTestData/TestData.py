class TestData:
    BASE_URL = "https://www.bit4you.io/"

    """LOGIN PAGE CREDENTIALS"""
    PhoneNumberOrEmail = "+12083016348"
    Pin = 123456

    """ABOUT US"""
    AboutUsPageTitle = "EU Exchange"

    """SUPPORT"""
    Support_Page_Heading = "We are here to help you"
    Name = "Ali"
    Email = "Aksam.ali786@gmail.com"
    Subject = "bit4you Rates"
    Message = "Hi, I need to know about bit4you rates. Thanks"
    EmailSentSuccessMessage = "Mail has been sent! We will contact you as soon as possible"
    EmailRestrictionMessage = "You can send maximum 10 emails per day using this form."
    AskAQuestionText = "crypto"

    SuggestedReadCryptoAsset = "CRYPTOS"
    SuggestedReadCryptoAssetMainHeading = "How can I buy a Bitcoin"

    FranciasText = "Commencez à échanger des cryptos actifs avec 0% de frais !"
    NederlandText = "Start nu met het uitwisselen van Crypto-actieven aan 0% kosten!"
    EnglishTextURL = "https://www.bit4you.io/"

    DiscoverTheAppOnAppleStore = "https://apps.apple.com/be/app/bit4you/id1444871913"
    DiscoverTheAppOnGoogleStore = "https://play.google.com/store/apps/details?id=com.bit4you.app"

    ExchangeAssetsAmount = 10
    BanContact_USTD_Main_Heading = "What is USDT ?"
    FiatWithdrawalDepositFee = "Deposit fees"
    FiatWithdrawalWithdrawalFee = "Withdrawal fees"
    FiatWithdrawalCrypto_to_cryptoFee = "Crypto-to-crypto exchange"

    MerchantToolMainHeading = "Merchant tools"
    ContactUsFormHeading = "Contact Us"

    NewsPageMainHeading = "Crypto news"

    BlockChainRevolutionMainHeading = "The blockchain revolution"
    SevenSurWebPageLink = "https://myprivacy.dpgmedia.be/consent/?siteKey=atXMVFeyFP1Ki09i&callbackUrl=https%3a%2f%2fwww.7sur7.be%2fprivacy-gate%2faccept-tcf2%3fredirectUri%3d%252f7s7%252ffr%252f1536%252fEconomie%252farticle%252fdetail%252f3469738%252f2018%252f08%252f29%252fLa-premiere-plateforme-belge-d-echange-de-crypto-actifs-est-lancee.dhtml"
    Bit4youFacebookPageLink = "https://www.facebook.com/bit4you.io/"
    Bit4youTwitterPageLink = "https://twitter.com/Bit4you1"
    Bit4youInstagramPageLink = "https://www.instagram.com/accounts/login/"
    Bit4youGitlabPageLink = "https://gitlab.com/bit4you"

    BuyingAmount = "10"
    BuyingCustomRate = "1"
    SellingAmount = "0.0000001"
    SellingCustomRate = "1"
    StopLossPercentage = "1"
    Wallets_First_Transaction_ID_BitcoinCash = "5dc209d017074aeeb6b73dac9f339725"

    EuroCurrency = "€"
    NederlandsLanguage = "AFMELDEN"
    ReferralAmount = "20"
